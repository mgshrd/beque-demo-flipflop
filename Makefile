COQPKG=FFDemo

MAJOR=0
MINOR=0
COMMITCOUNT=$(shell git log --oneline | wc -l)
PATCH=$(shell expr $(COMMITCOUNT) - 24)
EXTRA=

SUBTARGET=all

BEQUEINSTDIR=Beque

COQROOT =
EDITOR = $(COQROOT)coqide

COQC=$(COQROOT)coqc
COQCFLAGS=

COQCHK=$(COQROOT)coqchk
COQCHKFLAGS=-o -silent

COQDEP=$(COQROOT)coqdep
COQDEPFLAGS=

DOT=dot
DOTFLAGS=

COQMAKEFILE=$(COQROOT)coq_makefile 
COQMAKEFILEFLAGS=

INSTALL=install
INSTALLFLAGS=-D -p -m 644

ALMOSTVFILES=$(shell find src -name \*.v | grep -v '\\\#' | sed -e 's,^src/,,')
VFILES=$(addprefix src/, $(ALMOSTVFILES))

PROJFILES=$(subst $(realpath .)/,,$(shell find $(realpath .) -name \.coq-proj-*.deps -print))
PROJDIRS=$(dir $(PROJFILES))
PROJMAKES=$(addsuffix Makefile,$(PROJDIRS))
PROJCOQPROJS=$(addsuffix _CoqProject,$(PROJDIRS))
PROJBINS=$(addsuffix .bin/,$(PROJDIRS))

V=true

src/%.v-edit: .bin/src/%.vo
	$(EDITOR) src/$*.v

src/%.v-check: .bin/src/%.vo
	$(V) -n COQCHECK $*.v" "
	$(COQCHK) $(COQCHKFLAGS) `sed -e 's/#.*//' < _CoqProject` $(shell basename "$*")
	$(V)

%/.bin/:
	mkdir -p $@

.bin/src/%.v: src/%.v
	mkdir -p $(shell dirname $@)
	ln -s $(shell dirname $@ | sed -e 's/[^/][^/]*/../g')/$< $@

src/%.vo: .bin/src/%.vo
	cp $< $@

.bin/%.vo: .bin/%.v _CoqProject
	$(V) -n COQC $*.v" "
	$(COQC) $(COQCFLAGS) `sed -e 's/#.*//' < _CoqProject` $<
	$(V)

.bin/src/extract/%Haskell.v: .bin/src/extract/%.v
	sed -e 's/Haskell/Haskell/' < $< > $@ # noop, here only for symmetry

.bin/src/extract/%Ocaml.v: .bin/src/extract/%.v
	sed -e 's/Haskell/Ocaml/' < $< > $@

./%.hs: all #.bin/src/linker/%linkHaskell.vo
	$(MAKE) -C src/linker V=$(V) $@
	cp src/linker/$@ .

./%.mli ./%.ml: all #.bin/src/linker/%linkOcaml.vo
	$(MAKE) -C src/linker V=$(V) $@
	cp src/linker/$*.ml .

run-%-hs: %.hs
	$(V) RUN HS $*
	runhaskell -Isrc/linker $<

run-%-ml: %.ml
	$(V) RUN ML $*
	ocaml $<

src/%/.buildmark: src/%/Makefile src/%/_CoqProject $(find src/$* -name \*.v)
	$(MAKE) -C src/$* V=$(V) $(SUBTARGET)

src/%/.buildmark-symlinks: src/%/Makefile src/%/_CoqProject $(find src/$* -name \*.v)
	$(MAKE) -C src/$* V=$(V) symlinks

src/%/.buildmark-docs: src/%/Makefile src/%/_CoqProject $(find src/$* -name \*.v)
	$(MAKE) -C src/$* V=$(V) docs

src/%/.buildmark-renders: src/%/Makefile src/%/_CoqProject $(find src/$* -name \*.v)
	$(MAKE) -C src/$* V=$(V) renders

src/%/.cleanmark: src/%/Makefile src/%/_CoqProject $(find src/$* -name \*.v)
	$(MAKE) -C src/$* clean

all: $(addsuffix .buildmark,$(PROJDIRS))
infra: $(PROJMAKES) $(PROJCOQPROJS) $(PROJBINS) $(addsuffix .buildmark-symlinks,$(PROJDIRS))
subdocs: $(addsuffix .buildmark-docs,$(PROJDIRS))
subrenders: $(addsuffix .buildmark-renders,$(PROJDIRS))
run: run-app1.pp-hs run-app1eff.pp-hs run-app1.pp-ml run-app1eff.pp-ml
collect-%:
	find src -name \*.$* -type f -print0 | cpio --null -o | sed -e 's,/\.bin/,//////,g' | (mkdir $(COQPKG); cd $(COQPKG); cpio -id)
	tar czf $(COQPKG)-$*-$(MAJOR).$(MINOR).$(PATCH)$(EXTRA).tar.gz $(COQPKG)
	rm -rf $(COQPKG)

ci:
	grep -- '- make' .gitlab-ci.yml | sed -e 's/-//' | while read x; do $$x V=echo; done

clean: $(addsuffix .cleanmark,$(PROJDIRS))
	find -name \*.vo -execdir rm {} +
	rm -f *.hs src/extract/*.hs
	rm -f *.ml src/extract/*.ml
	rm -f *.mli src/extract/*.mli
	rm -f _CoqProject ._CoqProject.proofgeneral
	rm .proj-deps
	rm -rf .bin
	rm -f $(PROJCOQPROJS) $(PROJMAKES)

_CoqProject ._CoqProject.proofgeneral: Makefile $(shell find . -path ./.bin -prune -o -name \.coq-proj-*.deps -print)
	test -e $@ && $(V) GENCP $(shell find . -path ./.bin -prune -o -name \.coq-proj-*.deps -newer $@ -print) || $(V) GENCP new $@
	echo "# automatically generated, update Makefile instead" > $@
	echo -R $(BEQUEINSTDIR) Beque >> $@
	find src -name \.coq-proj-*.deps | sed -e 's,.coq-proj-,.bin/ ,' -e 's/.deps$$//' -e 's,^,-R '$(realpath .)/, >>$@
	# proofgeneral workaround: PG reverses the order of arguments when passing to coqtop, set coq-project-filename to "._CoqProject.proofgeneral" in your emacs config
	tac $@ > .$@.proofgeneral

src/%/Makefile: subprojectMakefile.mk src/%/_CoqProject
	$(V) GENSM $@
	sed -e 's,@@PROJHOME@@,$(realpath .),g' < $< > $@

src/%/_CoqProject src/%/._CoqProject.proofgeneral: src/%/.coq-proj-*.deps _CoqProject
	$(V) GENSCP $@
	echo "# automatically generated, update $(shell basename $<) instead" >$@
	test -n "$(realpath $(BEQUEINSTDIR))" || echo "BEQUEINSTDIR is set to '$(BEQUEINSTDIR)', but it can not be found" >&2
	test -n "$(realpath $(BEQUEINSTDIR))"
	echo -R $(realpath $(BEQUEINSTDIR)) Beque >> $@
	sed -e 's/#.*//' $< | grep . | while read p; do grep \ $$p\$$ _CoqProject; done >>$@
	echo $< | sed -e 's/.*coq-proj-//' -e 's/.deps$$//' -e "s/^/-R .bin /" >> $@
	# proofgeneral workaround: PG reverses the order of arguments when passing to coqtop, set coq-project-filename to "._CoqProject.proofgeneral" in your emacs config
	tac $@ > src/$*/._CoqProject.proofgeneral

ver:
	echo $(MAJOR).$(MINOR).$(PATCH)$(EXTRA)

CoqMakefile: $(VFILES)
	$(COQMAKEFILE) $(COQMAKEFILEFLAGS) $^ > $@

.proj-targets: _CoqProject Makefile $(PROJFILES)
	awk '/^-R.*bin\/ / { print "build-" $$3 ": " $$2 ".buildmark" }' $< | sed -e 's,$(realpath .)/,,g' -e 's,\.bin/,,' > $@

.proj-deps-%: .proj-deps
	sed -e 's,/\.buildmark,&-$*,' < $< > $@

.proj-deps: Makefile _CoqProject $(PROJFILES)
	for pd in $(PROJDIRS); do \
		echo -n $$pd.buildmark:; \
		cat $$pd/.coq-proj-*.deps | while read px; do \
			ln=$$((ln+1)); \
			p=`echo "$$px" | sed -e 's/#.*//'`; \
			if [ -z "$$p" ]; then continue; fi; \
			if grep -q $$p _CoqProject; then \
				echo -n \ `grep \ $$p\$$ _CoqProject | cut -d \  -f 2 | sed -e 's,^$(realpath .)/,,' -e 's,/.bin,,g'`.buildmark; \
			else \
				echo; \
				echo $$pd.coq-proj-*.deps :$$ln:error: project $$p not found; \
			fi \
		done; \
		echo; \
	done >$@.tmp
	grep ' :[0-9]*:error: ' $@.tmp | sed -e 's/\.deps :/.deps:/'
	! grep -q ' :[0-9]*:error: ' $@.tmp
	mv $@.tmp $@


deps.dot: _CoqProject $(VFILES)
	$(COQDEP) $(COQDEPFLAGS) -dumpgraph $@ `sed -e 's/#.*//' < _CoqProject | grep -v -- -R..bin/src.$(COQPKG)` -R src $(COQPKG) src > /dev/null

deps.png: deps.dot
	$(DOT) $(DOTFLAGS) -Tpng $< > $@

-include .proj-deps
-include .proj-deps-docs
-include .proj-deps-renders
-include .proj-deps-symlinks
-include .proj-targets
