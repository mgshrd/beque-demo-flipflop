COQPKG=$(shell ls .coq-proj-*.deps | sed -e 's/^.coq-proj-//' -e 's/.deps$$//')

COQROOT =
EDITOR = $(COQROOT)coqide

COQC=$(COQROOT)coqc
COQCFLAGS=$(shell sed -e 's/\#.*//' _CoqProject)

COQCHK=$(COQROOT)coqchk
COQCHKFLAGS=-o -silent

COQDEP=$(COQROOT)coqdep
COQDEPFLAGS=

COQDOC=$(COQROOT)coqdoc
COQDOCFLAGS=--external https://beque.gitlab.io/core/ Beque $(sed -e 's/\#.*//' _CoqProject)

PROVIOLAROOT=@@PROJHOME@@/proviola
PROVIOLA=python $(PROVIOLAROOT)/camera/camera.py
PROVIOLAFLAGS=--coqtop '$(COQROOT)coqtop' --coq-project _CoqProject --shift-response --include-command --response-diff

XSLTPROC=xsltproc
XSLTPROCFLAGS=

DOT=dot
DOTFLAGS=

COQMAKEFILE=$(COQROOT)coq_makefile 
COQMAKEFILEFLAGS=

INSTALL=install
INSTALLFLAGS=-D -p -m 644

VFILES=$(shell find . -path ./.bin -prune -o -name \*.v -print | grep -v '\\\#' | sed -e 's,^\./,,')
VOFILES=$(addprefix .bin/, $(addsuffix o, $(VFILES)))

V=true

.bin/%.v: %.v
	mkdir -p $(shell dirname $@)
	ln -s ../$(shell echo $(dir $<) | sed -e 's,^\./,,' -e 's,/\./,/,g' -e 's/[^/]\+/../g')$< $@

%.htm: %.flm
	$(V) XSLTPROC $<
	$(XSLTPROC) $(XSLTPROCFLAGS) $(PROVIOLAROOT)/proviola/coq/proviola.xsl $< > $@

%.flm: %.html
	$(V) PROVIOLA $<
	$(PROVIOLA) $(PROVIOLAFLAGS) $< $@

.bin/%.html: .bin/%.vo _CoqProject
	if [ ! -e $(notdir $@) -o $< -nt $(notdir $@) ]; then \
		$(V) COQDOC $<; \
		$(COQDOC) $(COQDOCFLAGS) $*.v -o $@; \
	else \
		$(V) MOVE COQDOC $<; \
		rm -f index.html; \
		rm -f coqdoc.css; \
		sed -e 's/This page has been generated by/$(COQPKG) - $(shell git rev-parse HEAD) - &/' < $(notdir $@) > $@; \
		rm $(notdir $@); \
	fi

.bin/%.vo: .bin/%.v _CoqProject
	$(V) -n COQC $*.v" "
	$(COQC) $(COQCFLAGS) $<
	$(V)

all: $(VOFILES)
symlinks: $(addprefix .bin/, $(VFILES))
validate: $(addsuffix -check, $(VFILES))
renders: $(VOFILES:.vo=.htm)
movies: $(VOFILES:.vo=.flm)
docs: $(VOFILES)
	$(V) COQDOC $(VOFILES:.vo=.v)
	$(COQDOC) $(COQDOCFLAGS) $(VOFILES:.vo=.v)

clean:
	find -name \*.vo -execdir rm {} +
	rm -f .coq-deps
	rm -rf .bin

.coq-deps: Makefile _CoqProject $(VFILES) $(shell sed -e 's/#.*//' < _CoqProject | grep -v -- -R..bin.$(COQPKG) | awk '{print $$2}')
	test -e .bin || mkdir .bin
	$(COQDEP) $(COQDEPFLAGS) `sed -e 's/#.*//' < _CoqProject | grep -v -- -R..bin.$(COQPKG)` -R . $(COQPKG) $(VFILES) | sed -e 's, \([^/][^ ]*\), .bin/\1,g' -e 's,^,.bin/,' -e 's, \./\.bin/, .bin/,g' > $@.tmp
	mv $@.tmp $@

-include .coq-deps
