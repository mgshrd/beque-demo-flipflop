Require Import List.
Require Import Omega.

Require Import sublist.
Require Import sublist_facts.
Require Import ltac_utils.
Require Import segmented_list.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.util.IODebug.

Require Import Frame.intf.DomainSegmentCanonicalProperties.

Require Import AxiomaticFFImpl.world_embedding.FFMapMessageToActionAndRes.
Require Import AxiomaticFFImpl.FF.

Module FFCanonicalProps
       (w : World)
       (ior : IORaw w)
       (m : FFMapMessageToActionAndRes w)
.

Module ff := FF w ior m.
Import ff.

Module iod := IODebug w ior.
Import iod.

Module x <: DomainSegmentCanonicalProperties w ior ff.

Lemma segment_not_empty :
  forall a res s,
    ff.segment_of_action_with_res a res s ->
    s <> nil.
Proof.
  intros. compute in H.
  destruct s; [ contradiction H | ].
  destruct s; [ | contradiction H ].
  intro d; discriminate d.
Qed.

Lemma segment_unambiguity :
  forall a res s,
    ff.segment_of_action_with_res a res s ->
    (forall a' res',
       a <> a' -> ~ff.segment_of_action_with_res a' res' s) /\
    (forall res',
       res <> res' -> ~ff.segment_of_action_with_res a res' s).
Proof.
  intros. destruct a.
  - ffseg H.
    unfold segment_of_action_with_res in H.
    magic_case (m.to_ff m); rewrite H0 in *; [ | contradiction H ].
    destruct s. destruct x; [ | contradiction H ].
    subst f. split; intros.
    + intro. destruct a'; [ elim H; reflexivity | ].
      simpl in res'. simpl in H1. rewrite H0 in H1. contradiction H1.
    + intro. simpl in res, res'.
      unfold segment_of_action_with_res in H1.
      rewrite H0 in H1.
      elim H. exact H1.
  - ffseg H.
    unfold segment_of_action_with_res in H.
    magic_case (m.to_ff m); rewrite H0 in *; [ | contradiction H ].
    destruct s. destruct x; [ contradiction H | ].
    clear H.
    split; intros; intro.
    + destruct a'; [ | elim H; reflexivity ].
      simpl in H1. rewrite H0 in H1. contradiction H1.
    + destruct res, res'; elim H; reflexivity.
Qed.

Lemma segment_entirety :
  forall a res s,
    ff.segment_of_action_with_res a res s ->
    (forall s', proper_sublist_of s' s -> forall a' res', ~ff.segment_of_action_with_res a' res' s').
Proof.
  intros. intro.
  destruct H0. destruct H0. destruct H0. destruct H2. subst s.
  ffseg H1.
  destruct x.
  - destruct x0.
    + elim H2. rewrite app_nil_r. reflexivity.
    + unfold segment_of_action_with_res in H. simpl in H. contradiction H.
  - destruct x; contradiction H.
Qed.

Corollary segment_entirety_cor :
  forall a res s,
    ff.segment_of_action_with_res a res s ->
    (forall s', proper_sublist_of s s' -> forall a' res', ~ff.segment_of_action_with_res a' res' s').
Proof.
  intros. intro. exact (segment_entirety _ _ _ H1 _ H0 _ _ H).
Qed.

Definition action_res_eq (a1 a2 : ff.action) (res1 : ff.action_res a1) (res2 : ff.action_res a2) : Prop.
Proof.
  destruct (ff.eq_action_dec a1 a2); [ | exact False ].
  subst a2. exact (res1 = res2).
Defined.

Lemma segment_no_recombine_before :
  forall a res s,
    ff.segment_of_action_with_res a res s ->
    forall p,
    proper_prefix_of p s ->
    forall a' res' s', ~ff.segment_of_action_with_res a' res' (s' ++ p).
Proof.
  intros. intro. ffseg H.
  exfalso. revert H0. clear. intro.
  destruct H0. destruct H. destruct H0.
  destruct x.
  { elim H0. rewrite <- H1. rewrite app_nil_r. reflexivity. }
  destruct p; [ elim H; reflexivity | ].
  simpl in H1. injection H1; clear H1; intros; subst.
  destruct p; discriminate H1.
Qed.

Lemma segment_no_recombine_after :
  forall a res s,
    ff.segment_of_action_with_res a res s ->
    forall p,
    proper_postfix_of p s ->
    forall a' res' s', ~ff.segment_of_action_with_res a' res' (p ++ s').
Proof.
  intros. intro.
  ffseg H.
  revert H0; clear; intro.
  destruct H0. destruct H as [ ? [ ? ? ] ].
  destruct x.
  - elim H0. exact H1.
  - injection H1; clear H1; intros; subst.
    destruct x; [ | discriminate H1 ].
    destruct p; [ elim H; reflexivity | ].
    discriminate H1.
Qed.

Lemma overlapping_segments :
  forall a1 res1 s1
         a2 res2 s2,
    ff.segment_of_action_with_res a1 res1 s1 ->
    ff.segment_of_action_with_res a2 res2 s2 ->
    proper_sublist_of s1 s2 ->
    False.
Proof.
  intros.
  assert (tmp1 := segment_entirety _ _ _ H0).
  assert (tmp2 := tmp1 _ H1). clear tmp1.
  exact (tmp2 _ _ H).
Qed.

Definition action_segmenter : Segmenter.
Proof.
  refine ({|
             segmenter_A := w.msg;
             segmenter_P := fun t => exists a res, ff.segment_of_action_with_res a res t;
           |}).

  {
    intros.
    destruct H. destruct H. destruct H0. destruct H0.
    assert (Hx := overlapping_segments _ _ _ _ _ _ H H0).
    elim Hx.
    exact H1.
  }

  {
    intros.
    destruct H. destruct H.
    assert (Hx := segment_not_empty _ _ _ H). exact Hx.
  }
Defined.

End x.

End FFCanonicalProps.
