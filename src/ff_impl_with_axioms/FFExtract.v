Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import AxiomaticFFImpl.world_embedding.FFMapMessageToActionAndRes.

Require Import AxiomaticFFImpl.FF.
Require Import AxiomaticFFImpl.FFCanonicalProps.

Module ffcpHaskell
       (w : World)
       (ior : IORaw w)
       (m : FFMapMessageToActionAndRes w)
.

Module ffcp := FFCanonicalProps w ior m.

Extract Constant ffcp.ff.ff_action_flip_ => "Prelude.putStrLn ""flip"" Prelude.>> (Prelude.return (Prelude.Right Prelude.True))".
Extract Constant ffcp.ff.ff_action_flop_ => "Prelude.putStrLn ""flop""".

End ffcpHaskell.

Module ffcpOcaml
       (w : World)
       (ior : IORaw w)
       (m : FFMapMessageToActionAndRes w)
.

Module ffcp.
Include FFCanonicalProps w ior m.
End ffcp.

Extract Constant ffcp.ff.ff_action_flip_ => "(fun () -> print_string ""flip\n""; Ok true)".
Extract Constant ffcp.ff.ff_action_flop_ => "(fun () -> print_string ""flop\n"")".

End ffcpOcaml.

