(** Link up domain implementations, user code, and a world of those domains. *)

Extraction Language Haskell.

Require Import Beque.Util.DelayExtractionDefinitionToken.
Require Import Beque.Util.common_extract.

Require Import App1.app1.

Require Import SingleFFWorld.SingleFF.
Require Import SingleFFWorld.SingleFFToFFOfElemFFImpl.

Require Import FFOfElemFFImpl.FFOfElemFFCanonicalProps.

Require Import AxiomaticElemFF.ElemFFExtract.

(* common things, like bool, list, etc mapped to their Haskell/Ocaml native representation *)
Module common_extract_activated.
Include commonExtractHaskell token.
End common_extract_activated.

(* world (IO) definitions & axiom extractions *)
Module singleff.
Include SingleFF.iorHaskell token.
End singleff.
Module ior.
Include singleff.ior.
End ior.

(* world to domain definition *)
Module mm.
Include SingleFFToFFOfElemFFImpl ior.
End mm.

(* domain definitions & axiom extractions *)
Module effcp_.
Include ElemFFExtract.effcpHaskell SingleFF ior mm.
End effcp_.
Module effcp.
Include effcp_.effcp.
End effcp.
Module ffeffcp.
Include FFOfElemFFCanonicalProps SingleFF ior effcp.eff effcp.x.
End ffeffcp.

(* link user app definition with world and domain *)
Module app1link.
Include app1 SingleFF ior ffeffcp.ff ffeffcp.x.
End app1link.

(* extrack linked app *)
Extraction "app1eff" app1link.main.
