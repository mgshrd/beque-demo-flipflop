(** Link up domain implementations, user code, and a world of those domains. *)

Extraction Language Haskell.

Require Import Beque.Util.DelayExtractionDefinitionToken.
Require Import Beque.Util.common_extract.

Require Import App1.app1.

Require Import SingleFFWorld.SingleFF.
Require Import SingleFFWorld.SingleFFToFFImplAxiom.

Require Import AxiomaticFFImpl.FFExtract.

(* common things, like bool, list, etc mapped to their Haskell/Haskell native representation *)
Module common_extract_activated.
Include commonExtractHaskell token.
End common_extract_activated.

(* world (IO) definitions & axiom extractions *)
Module singleff.
Include SingleFF.iorHaskell token.
End singleff.
Module ior.
   Include singleff.ior.
End ior.

(* world to domain definition *)
Module mm.
Include SingleFFToFFImplAxiom ior.
End mm.

(* domain definitions & axiom extractions *)
Module ffcp.
Include FFExtract.ffcpHaskell SingleFF ior mm.
End ffcp.

(* link user app definition with world and domain *)
Module app1link.
Include app1 SingleFF ior ffcp.ffcp.ff ffcp.ffcp.x.
End app1link.

(* extrack linked app *)
Extraction "app1" app1link.main.
