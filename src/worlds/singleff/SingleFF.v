Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.
Require Import IO.impl.AxiomaticIO.

Require Import FFModel.FFActions.

Require Import DelayExtractionDefinitionToken.

Module SingleFF
<: World
.

Definition msg := { a : ff_action & ff_action_res a }.

End SingleFF.

Module iorHaskell (delay : DelayExtractionDefinitionToken).
Module ior.
Include AxiomaticIO SingleFF.
End ior.
Include ior.extractHaskell delay.
End iorHaskell.

Module iorOcaml (delay : DelayExtractionDefinitionToken).
Module ior.
Include AxiomaticIO SingleFF.
End ior.
Include ior.extractOcaml delay.
End iorOcaml.
