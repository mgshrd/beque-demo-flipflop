Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import AxiomaticFFImpl.world_embedding.FFMapMessageToActionAndRes.

Require Import SingleFF.

Module SingleFFToFFImplAxiom (ior : IORaw SingleFF) <: FFMapMessageToActionAndRes SingleFF.

Module io := IOAux SingleFF ior.

Definition to_ff (m : SingleFF.msg) := Some m.

End SingleFFToFFImplAxiom.
