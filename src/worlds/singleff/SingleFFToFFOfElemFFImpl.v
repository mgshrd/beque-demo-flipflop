Require Import ltac_utils.

Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import Frame.util.UnitHandle.

Require Import ElemFFModel.ElemFFActions.

Require Import AxiomaticElemFF.world_embedding.ElemFFMapMessageToActionAndRes.

Require Import SingleFFWorld.SingleFF.

Module SingleFFToFFOfElemFFImpl (ior : IORaw SingleFF) <: ElemFFMapMessageToActionAndRes SingleFF UnitHandle.

Module io := IOAux SingleFF ior.

Module ffa := ElemFFActions.

Definition to_ff (m : SingleFF.msg) :
  option {a : ffa.ff_action & @ffa.ff_action_res UnitHandle.handle a}.
Proof.
  destruct m.
  magic_case x.
  - rewrite H in *. simpl in f.
    apply Some. refine (existT _ (ffa.ff_action_flip UnitHandle.h) _).
    simpl. exact f.
  - rewrite H in *. simpl in f.
    apply Some. refine (existT _ (ffa.ff_action_flop UnitHandle.h) _).
    simpl. exact f.
Defined.

End SingleFFToFFOfElemFFImpl.
