Require Import List.
Require Import Omega.

Require Import ltac_utils.
Require Import sublist.
Require Import result.
Require Import dec_utils.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import LTL.util.LTL.

Require Import Frame.intf.Domain.

Require Import FFModel.FFActions.

Module Type FFDomain
       (w : World)
       (ior : IORaw w)
<: Domain w ior
.

Module io := IOAux w ior.
Module ltl := LTL w.

Definition action := ff_action.

Definition eq_action_dec :
  forall a1 a2 : action,
    {a1 = a2}+{a1 <> a2}.
Proof. decide equality. Defined.

Definition action_res := ff_action_res.

Definition eq_action_res_dec :
  forall (a : action) (res1 res2 : action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  destruct a; simpl in *.
  - exact (eq_result_dec eq_bool_dec eq_nat_dec _ _).
  - exact (eq_unit_dec _ _).
Defined.

Inductive ff_prop : Set := ff_prop_isflip | ff_prop_isflop.
Definition prop := ff_prop.

Definition eq_prop_dec :
  forall p1 p2 : prop,
    {p1 = p2}+{p1 <> p2}.
Proof. decide equality. Defined.

(** Segments are the "message"s of a domain. *)
Parameter segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg),
    Prop.

Definition propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop :=
  fun a res p =>
    match a, res, p with
      | ff_action_flip, err _, ff_prop_isflop => True
      | _, _, _ => False
    end.

Definition propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros.
  destruct a, res, p; try solve [ right; intro H; exact H ].
  left. compute. exact I.
Defined.

Definition back_propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop :=
  fun a res p =>
    match a, res, p with
      | ff_action_flip, err _, ff_prop_isflop => True
      | _, _, _ => False
    end.

Definition back_propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros.
  destruct a, res, p; try solve [ right; intro H; exact H ].
  left. compute. exact I.
Defined.

Definition starts_with_partial_segment
           (s : list w.msg) :=
  forall a res s',
    prefix_of s' s ->
    ~segment_of_action_with_res a res s'.

Definition simple_propagation
           (a : action)
           (res : action_res a)
           (p : prop) :=
  ltl.tillnow
    (fun t =>
       exists s,
         prefix_of s t /\
         segment_of_action_with_res a res s)
    (fun t =>
       (* TODO partial segments assumed to propagate *)
       starts_with_partial_segment t \/
       (* the segment declared as propagating *)
       (exists a' res' s,
          prefix_of s t /\
          segment_of_action_with_res a' res' s /\
          propagates a' res' p)).

Definition prop_to_trace_prop :
  forall p : prop,
    list w.msg -> Prop :=
  fun p =>
    match p with
      | ff_prop_isflip =>
        ltl.tillnow
          (fun t =>
             exists s,
               prefix_of s t /\
               exists u, segment_of_action_with_res ff_action_flip (ok u) s)
          (fun t =>
             forall s,
               prefix_of s t ->
               ~segment_of_action_with_res ff_action_flop tt s)
      | ff_prop_isflop =>
        ltl.tillnow
          (fun t =>
             exists s,
               prefix_of s t /\
               segment_of_action_with_res ff_action_flop tt s)
          (fun t =>
             forall s,
               prefix_of s t ->
               ~exists u, segment_of_action_with_res ff_action_flip (ok u) s)
    end.

Definition isflip := prop_to_trace_prop ff_prop_isflip.
Definition isflop := prop_to_trace_prop ff_prop_isflop.

(** ff_action_flipflopness requrirement to implementors *)
Parameter ff_action_flip_xor_ff_action_flop :
  forall t,
    (prop_to_trace_prop ff_prop_isflip t -> ~prop_to_trace_prop ff_prop_isflop t) /\
    (prop_to_trace_prop ff_prop_isflop t -> ~prop_to_trace_prop ff_prop_isflip t).
    

Parameter flip :
  ior.IO
    (fun t => prop_to_trace_prop ff_prop_isflop t)
    (action_res ff_action_flip)
    (fun res s t =>
       segment_of_action_with_res ff_action_flip res s /\
       ((exists b, res = ok b) -> prop_to_trace_prop ff_prop_isflip (s ++ t))).

Parameter flop :
  ior.IO
    (fun t => prop_to_trace_prop ff_prop_isflip t)
    (action_res ff_action_flop)
    (fun res s t =>
       segment_of_action_with_res ff_action_flop res s /\
       prop_to_trace_prop ff_prop_isflop (s ++ t)).

(** Translate an action id into a IO action *)
Definition action_to_IO :
  forall (a : action),
    { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                         (snd pre_post) res s t ->
                         segment_of_action_with_res a res s))%type}.
Proof.
  intro. magic_case a.
  - refine (let Hx := io.IO_weaken
                        flip
                        (fun res s t => segment_of_action_with_res ff_action_flip res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); [ solve [ auto ] ].
    + intros. destruct H1 as [ H1 _ ]. exact H1.
  - refine (let Hx := io.IO_weaken
                        flop
                        (fun res s t => segment_of_action_with_res ff_action_flop res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); solve [ auto ].
    + intros. destruct H1 as [ H1 _ ]. exact H1.
Defined.

Conjecture propagates_correct :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p t ->
        prop_to_trace_prop p (s ++ t).

Conjecture back_propagates_correct :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p (s ++ t) ->
        prop_to_trace_prop p t.

Corollary neg_propagation :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p (s ++ t) ->
        ~prop_to_trace_prop p t.
Proof.
  intros. intro. elim H1.
  exact (propagates_correct H H0 H2).
Qed.

Corollary neg_back_propagation :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p t ->
        ~prop_to_trace_prop p (s ++ t).
Proof.
  intros. intro. elim H1. apply (back_propagates_correct H H0 H2).
Qed.

End FFDomain.
