Require Import result.

Inductive ff_action :
Set :=
| ff_action_flip
| ff_action_flop.

Definition ff_action_res :
  forall (a : ff_action),
    Set :=
  fun a =>
    match a with
      | ff_action_flip => result bool(*could be unit, stress test*) nat
      | ff_action_flop => unit
    end.
