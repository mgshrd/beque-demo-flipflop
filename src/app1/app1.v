Require Import String.
Require Import List.

Require Import ltac_utils.
Require Import result.
Require Import ltac_seg.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.util.IODebug.
Require Import IO.util.ResultUtils.

Require Import Frame.intf.DomainSegmentCanonicalProperties.

Require Import FFModel.FFDomain.

Module app1
       (w : World)
       (ior : IORaw w)
       (ffd : FFDomain w ior)
       (ffcp : DomainSegmentCanonicalProperties w ior ffd)
.

Import ffd.

Module iod := IODebug w ior.
Import iod.
Import iod.io.

Module ru := ResultUtils w ior.
Import ru.

Definition main : IO isflop unit (fun _ s t => isflop (s ++ t)%list).
Proof.
  unshelve refine (do? "fin" \
                     _ <-- IO_return 0;;
                     r <?--? "flip" \ flip
                     [err
                        _ <-- IO_return tt;;
                        /| IO_return r
                     ];;
                     _ <--? "flop" \ flop;;
                     _ <-- IO_return (r:bool);;
                     /|? "nopp" \ IO_return tt
                  ); try solve [ simpl; auto ].

  { intros. destruct H as [ _ [ ? [ ? ? ] ] ]. explode H. subst. exact H0. }

  {
    intros. explode H.
    sega. apply H2. eexists. reflexivity.
  }

  { intros. split; [ constructor | ]. intros. exact I. } 

  { intros. exact tt. }
  
  {
    intros. desig. sega. destruct x0; simpl in *.
    - desig. sega. simpl in *. subst u. exact H6.
    - sega. clear H5 H0. destruct H as [ _ H ]. 
      clear H2.
      rename x4 into failflip. rename t into floptrace.
      simpl.
      refine (ffd.propagates_correct _ H3 H). exact I.
  }
Defined.

End app1.
