Require Import List.
Require Import Arith.
Require Import Omega.

Require Import ltac_utils.
Require Import sublist.
Require Import sublist_facts.
Require Import dec_utils.
Require Import result.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import Frame.util.ExistentialAction.
Require Import Frame.intf.array.ElemHandle.
Require Import Frame.util.UnitHandle.

Require Import LTL.util.LTL.
Require Import LTL.util.LTLUtilFacts.

Require Import FFModel.FFActions.
Require Import FFModel.FFDomain.

Require ElemFFModel.ElemFFActions.
Require Import ElemFFModel.ElemFFDomain.

Module FFOfElemFF
       (w : World)
       (ior : IORaw w)
       (eff : ElemFFDomain w ior UnitHandle)
       (*m : ElemFFMapMessageToActionAndRes w UnitHandle*)
<: FFDomain w ior
.

Module ltl := LTL w.
Module ea := ExistentialAction w ior.

Module h := UnitHandle.
(*Module effcp :=  ElemFFCanonicalProps w ior h m.*)
(*Module io := effcp.ff.io.*)
Module io := eff.io.

Definition ff_action := ff_action.
Definition action := ff_action.
Definition to_elem_action a :=
  match a with
    | ff_action_flip => ElemFFActions.ff_action_flip h.h
    | ff_action_flop => ElemFFActions.ff_action_flop h.h
  end.
Definition from_elem_action a :=
  match a with
    | ElemFFActions.ff_action_flip tt => ff_action_flip 
    | ElemFFActions.ff_action_flop tt => ff_action_flop
  end.

Definition eq_action_dec :
  forall a1 a2 : action,
    {a1 = a2}+{a1 <> a2}.
Proof. decide equality. Defined.

Definition action_res := ff_action_res.

Definition eq_action_res_dec :
  forall (a : action) (res1 res2 : action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  assert (be := eq_bool_dec).
  assert (ne := eq_nat_dec).
  destruct a; simpl in *; decide equality.
Defined.

Lemma action_res_eq_eff_action_res :
  forall a,
    action_res a = eff.action_res (to_elem_action a).
Proof.
  intros; destruct a; compute; reflexivity.
Qed.

Definition to_elem_action_res a :
  action_res a -> eff.action_res (to_elem_action a)
  := ltac:(intros; destruct a; compute in *; exact H).
Definition from_elem_action_res a :
  eff.action_res (to_elem_action a) -> action_res a
  := ltac:(intros; destruct a; compute in *; exact H).

Inductive ff_prop : Set :=
| ff_prop_isflip
| ff_prop_isflop.
Definition prop := ff_prop.

Definition eq_prop_dec :
  forall p1 p2 : prop,
    {p1 = p2}+{p1 <> p2}.
Proof. decide equality. Defined.

Definition to_elem_prop p :=
  match p with
    | ff_prop_isflip => eff.ff_prop_isflip h.h
    | ff_prop_isflop => eff.ff_prop_isflop h.h
  end.
Definition from_elem_prop p :=
  match p with
    | eff.ff_prop_isflip tt => ff_prop_isflip
    | eff.ff_prop_isflop tt => ff_prop_isflop
  end.

(** Segments are the "message"s of a domain. *)
(* TODO mapping [m.to_ff h = None] to [False] is wrong, it is neither [True] nor [False]. *)
Definition segment_of_action_with_res
           (a : action)
           (res : action_res a)
           (s : list w.msg) :
  Prop :=
  eff.segment_of_action_with_res (to_elem_action a) ltac:(destruct a; auto) s.

Ltac ffseg H := match type of H with
                  | _ _ _ ?s =>
                    destruct s;
                      [ compute in H; contradiction H
                      | destruct s;
                        [ idtac
                        | compute in H; contradiction H
                        ]
                      ]
                end.

Definition propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop :=
  fun a res p =>
    match a, res, p with
      | ff_action_flip, err _, ff_prop_isflop => True
      | _, _, _ => False
    end.

Definition propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros.
  destruct a, res, p; try solve [ right; intro H; exact H ].
  left. compute. exact I.
Defined.

Definition back_propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop := propagates.

Definition back_propagates_dec :
  forall a res p,
    {back_propagates a res p}+{~back_propagates a res p} := propagates_dec.

Definition starts_with_partial_segment
           (s : list w.msg) :=
  forall a res s',
    prefix_of s' s ->
    ~segment_of_action_with_res a res s'.

Definition simple_propagation
           (a : action)
           (res : action_res a)
           (p : prop) :=
  ltl.tillnow
    (fun t =>
       exists s,
         prefix_of s t /\
         segment_of_action_with_res a res s)
    (fun t =>
       (* TODO partial segments assumed to propagate *)
       starts_with_partial_segment t \/
       (* the segment declared as propagating *)
       (exists a' res' s,
          prefix_of s t /\
          segment_of_action_with_res a' res' s /\
          propagates a' res' p)).

Definition prop_to_trace_prop :
  forall p : prop,
    list w.msg -> Prop :=
  fun p =>
    match p with
      | ff_prop_isflip =>
        ltl.tillnow
          (fun t =>
             exists s,
               prefix_of s t /\
               exists b, segment_of_action_with_res ff_action_flip (ok b) s)
          (fun t =>
             forall s,
               prefix_of s t ->
               ~segment_of_action_with_res ff_action_flop tt s)
      | ff_prop_isflop =>
        ltl.tillnow
          (fun t =>
             exists s,
               prefix_of s t /\
               segment_of_action_with_res ff_action_flop tt s)
          (fun t =>
             forall s,
               prefix_of s t ->
               ~exists b, segment_of_action_with_res ff_action_flip (ok b) s)
    end.

Module luf := LTLUtilFacts w.
(** flipflopness requrirement to implementors *)
Definition ff_action_flip_xor_ff_action_flop :
  forall t,
    (prop_to_trace_prop ff_prop_isflip t -> ~prop_to_trace_prop ff_prop_isflop t) /\
    (prop_to_trace_prop ff_prop_isflop t -> ~prop_to_trace_prop ff_prop_isflip t).
Proof.
  intros.
  assert (Hx := eff.ff_action_flip_xor_ff_action_flop UnitHandle.h t).
  firstorder.
Qed.

Lemma propagates_correct
           {p a res} (a_prop_p : propagates a res p)
           {s} (s_is_a : segment_of_action_with_res a res s)
           {t} (t_is_p : prop_to_trace_prop p t) :
  prop_to_trace_prop p (s ++ t).
Proof.
  intros.
  assert (Hx := fun a_prop_p' s_is_a' t_is_p' =>
                  @eff.propagates_correct
                    (to_elem_prop p) (to_elem_action a) (to_elem_action_res _ res) a_prop_p'
                    s s_is_a'
                    t t_is_p').
  destruct a, res, p;
    [ firstorder
    | firstorder
    | firstorder
    | idtac
    | firstorder
    | firstorder
    ].
  match type of Hx with
      ?x -> ?y -> ?z -> _
      =>
      assert (x) as H';
        [ firstorder
        | assert (y) as H0';
          [ firstorder
          | assert (z) as H1';
            [ firstorder
            |
            ]
          ]
        ];
        assert (Hx' := Hx H' H0' H1'); clear Hx H' H0' H1'
  end.
  firstorder.
Qed.

Lemma back_propagates_correct
           {p a res} (a_prop_p : back_propagates a res p)
           {s} (s_is_a : segment_of_action_with_res a res s)
           {t} (t_is_p : prop_to_trace_prop p (s ++ t)) :
  prop_to_trace_prop p t.
Proof.
  intros.
  assert (Hx := fun a_prop_p' s_is_a' t_is_p' =>
                  @eff.back_propagates_correct
                    (to_elem_prop p) (to_elem_action a) (to_elem_action_res _ res) a_prop_p'
                    s s_is_a'
                    t t_is_p').
  destruct a, res, p;
    [ firstorder
    | firstorder
    | firstorder
    | idtac
    | firstorder
    | firstorder
    ].
  match type of Hx with
      ?x -> ?y -> ?z -> _
      =>
      assert (x) as H';
        [ firstorder
        | assert (y) as H0';
          [ firstorder
          | assert (z) as H1';
            [ firstorder
            |
            ]
          ]
        ];
        assert (Hx' := Hx H' H0' H1'); clear Hx H' H0' H1'
  end.
  firstorder.
Qed.

Definition isflip := prop_to_trace_prop ff_prop_isflip.
Definition isflop := prop_to_trace_prop ff_prop_isflop.

Definition flip :
  ior.IO
    (fun t : list w.msg => prop_to_trace_prop ff_prop_isflop t)
    (action_res ff_action_flip)
    (fun (res : action_res ff_action_flip) (s t : list w.msg) =>
       segment_of_action_with_res ff_action_flip res s /\
       ((exists b, res = ok b) -> prop_to_trace_prop ff_prop_isflip (s ++ t)))
  := eff.flip UnitHandle.h.

Definition flop :
  ior.IO
    (fun t : list w.msg => prop_to_trace_prop ff_prop_isflip t)
    (action_res ff_action_flop)
    (fun (res : action_res ff_action_flop) (s t : list w.msg) =>
       segment_of_action_with_res ff_action_flop res s /\
       prop_to_trace_prop ff_prop_isflop (s ++ t))
  := eff.flop UnitHandle.h.

Definition action_to_IO :
  forall (a : action),
    { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                         (snd pre_post) res s t ->
                         segment_of_action_with_res a res s))%type}.
Proof.
  intro. magic_case a.
  - refine (let Hx := io.IO_weaken
                        flip
                        (fun res s t => segment_of_action_with_res ff_action_flip res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); [ solve [ auto ] ].
    + intros. destruct H1 as [ H1 _ ]. exact H1.
  - refine (let Hx := io.IO_weaken
                        flop
                        (fun res s t => segment_of_action_with_res ff_action_flop res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); solve [ auto ].
    + intros. destruct H1 as [ H1 _ ]. exact H1.
Defined.

Corollary neg_propagation :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p (s ++ t) ->
        ~prop_to_trace_prop p t.
Proof.
  intros. intro. elim H1.
  exact (propagates_correct H H0 H2).
Qed.

Corollary neg_back_propagation :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p t ->
        ~prop_to_trace_prop p (s ++ t).
Proof.
  intros. intro. elim H1. apply (back_propagates_correct H H0 H2).
Qed.

End FFOfElemFF.
