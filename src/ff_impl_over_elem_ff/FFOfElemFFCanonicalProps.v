Require Import List.
Require Import Omega.

Require Import sublist.
Require Import sublist_facts.
Require Import ltac_utils.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.util.IODebug.

Require Import Frame.intf.DomainSegmentCanonicalProperties.

Require Import ElemFFModel.ElemFFDomain.

Require Import FFOfElemFFImpl.world_embedding.FFMapMessageToActionAndRes.
Require Import FFOfElemFFImpl.FFOfElemFF.

Require Import UnitHandle.

Module FFOfElemFFCanonicalProps
       (w : World)
       (ior : IORaw w)
       (eff : ElemFFDomain w ior UnitHandle)
       (effcp : DomainSegmentCanonicalProperties w ior eff)
.


Module ff := FFOfElemFF w ior eff.
Import ff.

Module iod := IODebug w ior.
Import iod.

Module x <: DomainSegmentCanonicalProperties w ior ff.

Lemma segment_not_empty :
  forall a res s,
    segment_of_action_with_res a res s ->
    s <> nil.
Proof.
  intros.
  apply (effcp.segment_not_empty (ff.to_elem_action a) (ff.to_elem_action_res _ res)).
  unfold segment_of_action_with_res in H.
  exact H.
Qed.

Lemma segment_unambiguity :
  forall a res s,
    segment_of_action_with_res a res s ->
    (forall a' res',
       a <> a' -> ~segment_of_action_with_res a' res' s) /\
    (forall res',
       res <> res' -> ~segment_of_action_with_res a res' s).
Proof.
  intros.
  assert (Hx := effcp.segment_unambiguity (ff.to_elem_action a) (ff.to_elem_action_res _ res) s).
  match type of Hx with ?x -> _ => assert (x) as H' end.
  { clear Hx. exact H. }
  assert (Hx' := Hx H). clear Hx H'.
  destruct Hx'.
  split; [ clear H1 | clear H0 ]; intros.
  - intro. apply (H0 (to_elem_action a') (to_elem_action_res _ res')).
    + intro. elim H1. unfold to_elem_action in H3. destruct a, a'; try discriminate H3; reflexivity.
    + exact H2.
  - intro. apply (H1 (to_elem_action_res _ res')).
    + intro. elim H0. destruct a, res, res';
        first
          [ discriminate H3
          | reflexivity
          | injit H3; reflexivity
          ].
    + exact H2.
Qed.

Lemma segment_entirety :
  forall a res s,
    segment_of_action_with_res a res s ->
    (forall s', proper_sublist_of s' s -> forall a' res', ~segment_of_action_with_res a' res' s').
Proof.
  intros.
  apply (effcp.segment_entirety (to_elem_action a) (to_elem_action_res _ res) s H _ H0).
Qed.

Corollary segment_entirety_cor :
  forall a res s,
    segment_of_action_with_res a res s ->
    (forall s', proper_sublist_of s s' -> forall a' res', ~segment_of_action_with_res a' res' s').
Proof.
  intros. intro. exact (segment_entirety _ _ _ H1 _ H0 _ _ H).
Qed.

Definition action_res_eq (a1 a2 : action) (res1 : action_res a1) (res2 : action_res a2) : Prop.
Proof.
  destruct (eq_action_dec a1 a2); [ | exact False ].
  subst a2. exact (res1 = res2).
Defined.

Lemma segment_no_recombine_before :
  forall a res s,
    segment_of_action_with_res a res s ->
    forall p,
    proper_prefix_of p s ->
    forall a' res' s', ~segment_of_action_with_res a' res' (s' ++ p).
Proof.
  intros.
  apply (effcp.segment_no_recombine_before (to_elem_action a) (to_elem_action_res _ res) s H p H0).
Qed.

Lemma segment_no_recombine_after :
  forall a res s,
    segment_of_action_with_res a res s ->
    forall p,
    proper_postfix_of p s ->
    forall a' res' s', ~segment_of_action_with_res a' res' (p ++ s').
Proof.
  intros.
  apply (effcp.segment_no_recombine_after (to_elem_action a) (to_elem_action_res _ res) s H p H0).
Qed.

Lemma overlapping_segments :
  forall a1 res1 s1
         a2 res2 s2,
    segment_of_action_with_res a1 res1 s1 ->
    segment_of_action_with_res a2 res2 s2 ->
    proper_sublist_of s1 s2 ->
    False.
Proof.
  intros.
  assert (tmp1 := segment_entirety _ _ _ H0).
  assert (tmp2 := tmp1 _ H1). clear tmp1.
  exact (tmp2 _ _ H).
Qed.

End x.

End FFOfElemFFCanonicalProps.
