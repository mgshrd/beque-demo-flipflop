Require Import IO.intf.World.

Require Import FFModel.FFActions.

(** Interface between the world and FFDomain implementations. *)
Module Type FFMapMessageToActionAndRes
       (w : World)
.

Parameter to_ff :
  w.msg ->
  option {a : _ & ff_action_res a}.

End FFMapMessageToActionAndRes.
