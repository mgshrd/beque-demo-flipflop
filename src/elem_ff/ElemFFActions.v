Require Import result.

Inductive ff_action {handle : Set} :
  Set :=
| ff_action_flip : handle -> @ff_action handle
| ff_action_flop : handle -> @ff_action handle.

Definition ff_action_res
           {h : Set}
           (a : @ff_action h) :
    Set :=
    match a with
    | ff_action_flip _ => result bool(* no real info here, just testing the limits *) nat
    | ff_action_flop _ => unit
    end.
