Require Import List.
Require Import Omega.

Require Import ltac_utils.
Require Import sublist.
Require Import dec_utils.
Require Import result.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import LTL.util.LTL.

Require Import Frame.intf.array.ElemHandle.
Require Import Frame.intf.array.ElemDomain.

Require Import ElemFFModel.ElemFFActions.

Module Type ElemFFDomain
       (w : World)
       (ior : IORaw w)
       (id : ElemHandle)
<: ElemDomain w ior id
.

Module io := IOAux w ior.
Module ltl := LTL w.

Definition action := @ff_action id.handle.
Definition action_for (a : action) :=
  match a with
    | ff_action_flip h => h
    | ff_action_flop h => h
  end.

Definition eq_action_dec :
  forall a1 a2 : action,
    {a1 = a2}+{a1 <> a2}.
Proof. decide equality; exact (id.eq_handle_dec _ _). Defined.

Definition action_res := @ff_action_res id.handle.

Definition eq_action_res_dec :
  forall (a : action) (res1 res2 : action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  assert (be := eq_bool_dec).
  assert (ne := eq_nat_dec).
  intros. destruct a; simpl in *; decide equality.
Defined.


Inductive ff_prop : Set :=
| ff_prop_isflip : id.handle -> ff_prop
| ff_prop_isflop : id.handle -> ff_prop.
Definition prop := ff_prop.
Definition prop_for p :=
  match p with
    | ff_prop_isflip h => h
    | ff_prop_isflop h => h
  end.


Definition eq_prop_dec :
  forall p1 p2 : prop,
    {p1 = p2}+{p1 <> p2}.
Proof. decide equality; exact (id.eq_handle_dec _ _). Defined.

(** Segments are the "message"s of a domain. *)
Parameter segment_of_action_with_res :
  forall
    (a : action)
    (res : action_res a)
    (s : list w.msg),
    Prop.

Definition propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop :=
  fun a res p =>
    match a, res, p with
      | ff_action_flip ah, (err _), ff_prop_isflop ph => ah = ph
      | _, _, _ => False
    end.

Definition propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros.
  destruct a, res, p, (id.eq_handle_dec h h0); subst;
  try solve [ right; intro; elim H; reflexivity ].
  - compute. left; reflexivity.
  - compute. right; exact n0.
Defined.

Definition back_propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop := propagates.

Definition back_propagates_dec :
  forall a res p,
    {back_propagates a res p}+{~back_propagates a res p} :=
  propagates_dec.

Definition starts_with_partial_segment
           (s : list w.msg) :=
  forall a res s',
    prefix_of s' s ->
    ~segment_of_action_with_res a res s'.

Definition simple_propagation
           (a : action)
           (res : action_res a)
           (p : prop) :=
  ltl.tillnow
    (fun t =>
       exists s,
         prefix_of s t /\
         segment_of_action_with_res a res s)
    (fun t =>
       (* TODO partial segments assumed to propagate *)
       starts_with_partial_segment t \/
       (* the segment declared as propagating *)
       (exists a' res' s,
          prefix_of s t /\
          segment_of_action_with_res a' res' s /\
          propagates a' res' p)).

Definition prop_to_trace_prop :
  forall p : prop,
    list w.msg -> Prop :=
  fun p =>
    match p with
      | ff_prop_isflip ph =>
        ltl.tillnow
          (fun t =>
             exists s,
               prefix_of s t /\
               exists b, segment_of_action_with_res (ff_action_flip ph) (ok b) s)
          (fun t =>
             forall s,
               prefix_of s t ->
               ~segment_of_action_with_res (ff_action_flop ph) tt s)
      | ff_prop_isflop ph =>
        ltl.tillnow
          (fun t =>
             exists s,
               prefix_of s t /\
               segment_of_action_with_res (ff_action_flop ph) tt s)
          (fun t =>
             forall s,
               prefix_of s t ->
               ~exists b, segment_of_action_with_res (ff_action_flip ph) (ok b) s)
    end.

Definition isflip h := prop_to_trace_prop (ff_prop_isflip h).
Definition isflop h := prop_to_trace_prop (ff_prop_isflop h).

(** ff_action_flipflopness requrirement to implementors *)
Parameter ff_action_flip_xor_ff_action_flop :
  forall h t,
    (prop_to_trace_prop (ff_prop_isflip h) t -> ~prop_to_trace_prop (ff_prop_isflop h) t) /\
    (prop_to_trace_prop (ff_prop_isflop h) t -> ~prop_to_trace_prop (ff_prop_isflip h) t).
    

Parameter flip :
  forall h,
    ior.IO
      (fun t => prop_to_trace_prop (ff_prop_isflop h) t)
      (action_res (ff_action_flip h))
      (fun res s t =>
         segment_of_action_with_res (ff_action_flip h) res s /\
         ((exists b, res = ok b)-> prop_to_trace_prop (ff_prop_isflip h) (s ++ t))).

Parameter flop :
  forall h,
    ior.IO
      (fun t => prop_to_trace_prop (ff_prop_isflip h) t)
      (action_res (ff_action_flop h))
      (fun res s t =>
         segment_of_action_with_res (ff_action_flop h) res s /\
         prop_to_trace_prop (ff_prop_isflop h) (s ++ t)).

(** Translate an action id into a IO action *)
Definition action_to_IO :
  forall (a : action),
    { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                         (snd pre_post) res s t ->
                         segment_of_action_with_res a res s))%type}.
Proof.
  intro. magic_case a.
  - refine (let Hx := io.IO_weaken
                        (flip h)
                        (fun res s t => segment_of_action_with_res (ff_action_flip h) res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); [ solve [ auto ] ].
    + intros. destruct H1 as [ H1 _ ]. exact H1.
  - refine (let Hx := io.IO_weaken
                        (flop h)
                        (fun res s t => segment_of_action_with_res (ff_action_flop h) res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); solve [ auto ].
    + intros. destruct H1 as [ H1 _ ]. exact H1.
Defined.

Conjecture propagates_correct :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p t ->
        prop_to_trace_prop p (s ++ t).

Conjecture back_propagates_correct :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        prop_to_trace_prop p (s ++ t) ->
        prop_to_trace_prop p t.

Corollary neg_propagation :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p (s ++ t) ->
        ~prop_to_trace_prop p t.
Proof.
  intros. intro. elim H1.
  exact (propagates_correct H H0 H2).
Qed.

Corollary neg_back_propagation :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p t ->
        ~prop_to_trace_prop p (s ++ t).
Proof.
  intros. intro. elim H1. apply (back_propagates_correct H H0 H2).
Qed.

End ElemFFDomain.
