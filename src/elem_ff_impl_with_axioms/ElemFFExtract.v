Require Import IO.intf.World.
Require Import IO.intf.IORaw.

Require Import UnitHandle.

Require Import AxiomaticElemFF.world_embedding.ElemFFMapMessageToActionAndRes.

Require Import AxiomaticElemFF.ElemFF.
Require Import AxiomaticElemFF.ElemFFCanonicalProps.

Module effcpHaskell
       (w : World)
       (ior : IORaw w)
       (m : ElemFFMapMessageToActionAndRes w UnitHandle)
.

Module effcp := ElemFFCanonicalProps w ior UnitHandle m.

Extract Constant effcp.eff.ff_action_flip_ => "\h -> (Prelude.putStrLn Prelude.$ ""flip"" Prelude.++ (Prelude.show h)) Prelude.>> (Prelude.return (Prelude.Right Prelude.True))".
Extract Constant effcp.eff.ff_action_flop_ => "\h -> Prelude.putStrLn Prelude.$ ""flop"" Prelude.++ (Prelude.show h)".

End effcpHaskell.

Module effcpOcaml
       (w : World)
       (ior : IORaw w)
       (m : ElemFFMapMessageToActionAndRes w UnitHandle)
.

Module effcp.
Include ElemFFCanonicalProps w ior UnitHandle m.
End effcp.

Extract Constant effcp.eff.ff_action_flip_ => "(fun () -> (fun _ -> print_string (""flip"" ^ ""()"" ^ ""\n""); Ok true))".
Extract Constant effcp.eff.ff_action_flop_ => "(fun () -> (fun _ -> print_string (""flop"" ^ ""()"" ^ ""\n"")))".

End effcpOcaml.

