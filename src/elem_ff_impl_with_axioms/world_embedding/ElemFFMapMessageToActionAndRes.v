Require Import IO.intf.World.

Require Import Frame.intf.array.ElemHandle.

Require Import ElemFFModel.ElemFFActions.

(** Interface between the world and FFDomain implementations. *)
Module Type ElemFFMapMessageToActionAndRes
       (w : World)
       (id : ElemHandle)
.

Parameter to_ff :
  w.msg ->
  option {a : _ & @ff_action_res id.handle a}.

End ElemFFMapMessageToActionAndRes.
