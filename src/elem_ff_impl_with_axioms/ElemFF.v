Require Import List.
Require Import Arith.
Require Import Omega.

Require Import ltac_utils.
Require Import sublist.
Require Import sublist_facts.
Require Import result.
Require Import dec_utils.

Require Import IO.intf.World.
Require Import IO.intf.IORaw.
Require Import IO.intf.IOAux.

Require Import Frame.util.ExistentialAction.
Require Import LTL.util.LTL.
Require Import LTL.util.LTLUtilFacts.

Require Import Frame.intf.array.ElemHandle.

Require Import ElemFFModel.ElemFFActions.
Require Import ElemFFModel.ElemFFDomain.

Require Import AxiomaticElemFF.world_embedding.ElemFFMapMessageToActionAndRes.

Module ElemFF
       (w : World)
       (ior : IORaw w)
       (id : ElemHandle)
       (m : ElemFFMapMessageToActionAndRes w id)
<: ElemFFDomain w ior id
.

Module io := IOAux w ior.
Module ltl := LTL w.
Module ea := ExistentialAction w ior.

Definition ff_action := @ff_action id.handle.
Definition action := ff_action.
Definition action_for (a : ff_action) :=
  match a with
    | ff_action_flip h => h
    | ff_action_flop h => h
  end.

Definition eq_action_dec :
  forall a1 a2 : action,
    {a1 = a2}+{a1 <> a2}.
Proof. decide equality; exact (id.eq_handle_dec _ _). Defined.

Definition action_res := @ff_action_res id.handle.

Definition eq_action_res_dec :
  forall (a : action) (res1 res2 : action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  assert (be := eq_bool_dec).
  assert (ne := eq_nat_dec).
  intros. destruct a; simpl in *; decide equality.
Defined.


Inductive ff_prop : Set :=
| ff_prop_isflip : id.handle -> ff_prop
| ff_prop_isflop : id.handle -> ff_prop.
Definition prop := ff_prop.
Definition prop_for p :=
  match p with
    | ff_prop_isflip h => h
    | ff_prop_isflop h => h
  end.

Definition eq_prop_dec :
  forall p1 p2 : prop,
    {p1 = p2}+{p1 <> p2}.
Proof. decide equality; exact (id.eq_handle_dec _ _). Defined.

(** Segments are the "message"s of a domain. *)
(* TODO mapping [m.to_ff h = None] to [False] is wrong, it is neither [True] nor [False]. *)
Definition segment_of_action_with_res
           (a : action)
           (res : action_res a)
           (s : list w.msg) :
  Prop :=
  match s with
    | h::nil =>
      match m.to_ff h, a return action_res a -> Prop with
        | Some (existT _ (ff_action_flip ah) res'), ff_action_flip ah' =>
          fun rx => ah = ah' /\ res' = rx
        | Some (existT _ (ff_action_flop ah) _), ff_action_flop ah' =>
          fun _ => ah = ah'
        | _, _ =>
          fun _ => False
      end res
    | _ =>
      False
  end.

Ltac ffseg H := match type of H with
                  | exists a, segment_of_action_with_res _ _ _ =>
                    destruct H as [ ? H ]; ffseg H
                  | segment_of_action_with_res _ _ ?s =>
                    destruct s;
                      [ compute in H; contradiction H
                      | destruct s;
                        [ idtac
                        | compute in H; contradiction H
                        ]
                      ]
                end.

Definition propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop :=
  fun a res p =>
    match a, res, p with
      | ff_action_flip ah, err _, ff_prop_isflop ph => ah = ph
      | _, _, _ => False
    end.

Definition propagates_dec :
  forall a res p,
    {propagates a res p}+{~propagates a res p}.
Proof.
  intros.
  destruct a, res, p, (id.eq_handle_dec h h0); subst;
  try solve [ right; intro; elim H; reflexivity ].
  - compute. left; reflexivity.
  - compute. right; exact n0.
Defined.

Definition back_propagates :
  forall
    (a : action)
    (res : action_res a)
    (p : prop),
    Prop := propagates.

Definition back_propagates_dec :
  forall a res p,
    {back_propagates a res p}+{~back_propagates a res p} := propagates_dec.

Definition starts_with_partial_segment
           (s : list w.msg) :=
  forall a res s',
    prefix_of s' s ->
    ~segment_of_action_with_res a res s'.

Definition simple_propagation
           (a : action)
           (res : action_res a)
           (p : prop) :=
  ltl.tillnow
    (fun t =>
       exists s,
         prefix_of s t /\
         segment_of_action_with_res a res s)
    (fun t =>
       (* TODO partial segments assumed to propagate *)
       starts_with_partial_segment t \/
       (* the segment declared as propagating *)
       (exists a' res' s,
          prefix_of s t /\
          segment_of_action_with_res a' res' s /\
          propagates a' res' p)).

Definition prop_to_trace_prop :
  forall p : prop,
    list w.msg -> Prop :=
  fun p =>
    match p with
      | ff_prop_isflip ph =>
        ltl.tillnow
          (fun t =>
             exists s,
               prefix_of s t /\
               exists b, segment_of_action_with_res (ff_action_flip ph) (ok b) s)
          (fun t =>
             forall s,
               prefix_of s t ->
               ~segment_of_action_with_res (ff_action_flop ph) tt s)
      | ff_prop_isflop ph =>
        ltl.tillnow
          (fun t =>
             exists s,
               prefix_of s t /\
               segment_of_action_with_res (ff_action_flop ph) tt s)
          (fun t =>
             forall s,
               prefix_of s t ->
               ~exists b, segment_of_action_with_res (ff_action_flip ph) (ok b) s)
    end.

(* TODO this should be determined by [World],
needs three valued props (True, False, DontKnow), so as to allow stops <-> ~keeps, which now breaks, b/c DK is mapped to False by [segment_of_action_with_res] *)
Lemma oof_propagates :
  forall p m t,
    prop_to_trace_prop p t ->
    m.to_ff m = None ->
    prop_to_trace_prop p (m::t).
Proof.
  intros. unfold prop_to_trace_prop in H.
  destruct p.
  - destruct H. destruct H. destruct H.
    exists (m::x). exists x0.
    split.
    { simpl. rewrite H. reflexivity. }
    split.
    { destruct H1. exact H1. }
    destruct H1. clear H1.
    intros. intro.
    assert (Hx := postfix_of_cons _ _ _ H1).
    destruct Hx.
    { exact (H2 _ H5 _ H3 H4). }
    subst s'. clear H1.
    simpl in H3.
    destruct H3. destruct s.
    { compute in H4. contradiction H4. }

    simpl in H1. injection H1; clear H1. intros. subst m0.
    unfold segment_of_action_with_res in H4. rewrite H0 in H4.
    destruct s; contradiction H4.
  - (*exactly as the previus case*)
    destruct H. destruct H. destruct H.
    exists (m::x). exists x0.
    split.
    { simpl. rewrite H. reflexivity. }
    split.
    { destruct H1. exact H1. }
    destruct H1. clear H1.
    intros. intro.
    assert (Hx := postfix_of_cons _ _ _ H1).
    destruct Hx.
    { exact (H2 _ H5 _ H3 H4). }
    subst s'. clear H1.
    simpl in H3.
    destruct H3. destruct s.
    { compute in H4. destruct H4. contradiction. }

    simpl in H1. injection H1; clear H1. intros. subst m0.
    unfold segment_of_action_with_res in H4. rewrite H0 in H4.
    destruct s; destruct H4; contradiction.
Qed.

(* TODO this should be determined by [World], see comment at [oof_propagates] *)
Lemma oof_propagates2 :
  forall p m t,
    prop_to_trace_prop p (m::t) ->
    m.to_ff m = None ->
    prop_to_trace_prop p t.
Proof.
  intros. unfold prop_to_trace_prop in H. destruct p.
  - destruct H. destruct H. destruct H. destruct H1.
    destruct x; [ destruct x0; [ discriminate H | ] | ].
    + injection H. clear H. intros. subst.
      destruct H1. destruct x.
      { exfalso. destruct H. destruct H1. destruct H1. }
      destruct H. destruct H.
      exists x. exists x0.
      injection H. clear H. intros. subst.
      split; [ reflexivity | ].
      compute in H1. destruct H1 as [ ? H1 ]. destruct x; [ | contradiction H1 ].
      rewrite H0 in H1. contradiction H1.
    + exists x. exists x0.
      injection H; clear H; intros; subst.
      split; [ reflexivity | ].
      split; [ exact H1 | ].
      intros.
      destruct H. subst x.
      assert (H2' := H2 s'). clear H2.
      apply H2'.
      * exists (m::x1). reflexivity.
      * exact H3.

  - destruct H. destruct H. destruct H. destruct H1.
    destruct x; [ destruct x0; [ discriminate H | ] | ].
    + injection H. clear H. intros. subst.
      destruct H1. destruct x.
      { exfalso. destruct H. destruct H1. }
      destruct H. destruct H.
      exists x. exists x0.
      injection H. clear H. intros. subst.
      split; [ reflexivity | ].
      compute in H1. destruct x; [ | contradiction H1 ].
      rewrite H0 in H1. contradiction H1.
    + exists x. exists x0.
      injection H; clear H; intros; subst.
      split; [ reflexivity | ].
      split; [ exact H1 | ].
      intros.
      destruct H. subst x.
      assert (H2' := H2 s'). clear H2.
      apply H2'.
      * exists (m::x1). reflexivity.
      * exact H3.
Qed.

Module luf := LTLUtilFacts w.
(** flipflopness requrirement to implementors *)
Definition ff_action_flip_xor_ff_action_flop :
  forall h t,
    (prop_to_trace_prop (ff_prop_isflip h) t -> ~prop_to_trace_prop (ff_prop_isflop h) t) /\
    (prop_to_trace_prop (ff_prop_isflop h) t -> ~prop_to_trace_prop (ff_prop_isflip h) t).
Proof.
  intros. split; intro; intro.

  - induction t.
    {
      unfold prop_to_trace_prop, ltl.tillnow in H.
      destruct H. destruct H. destruct H. nilapp H.
      destruct H1. destruct H1. destruct H1. destruct H1. nilapp H1.
      compute in H3. destruct H3. contradiction.
    }

    destruct (luf.tillnow_unfold _ _ _ _ H) as [ flipstarts | flipkeeps ]; clear H;
      destruct (luf.tillnow_unfold _ _ _ _ H0) as [ flopstarts | flopkeeps ]; clear H0.

    + destruct flipstarts. destruct H. destruct flopstarts. destruct H1.
      ffseg H0.
      ffseg H2.
      compute in H0. compute in H2.
      magic_case (m.to_ff m); rewrite H3 in *; [ | contradiction H0 ].
      magic_case (m.to_ff m0); rewrite H4 in *; [ | contradiction H2 ].
      destruct s0, s; destruct x, x0; try contradiction.
      destruct H; injection H; clear H; intros; subst.
      destruct H1; injection H; clear H; intros; subst.
      rewrite H3 in H4.
      discriminate H4.

    + destruct flipstarts. destruct H.
      ffseg H0. rename H0 into mflip.
      destruct H. injection H; clear H; intros; subst. rename a into m.
      destruct flopkeeps.
      clear H.
      rename H0 into mnotfliptrue.
      assert (mnotfliptrue' := mnotfliptrue (m::nil)). clear mnotfliptrue.
      apply mnotfliptrue'.
      * exists t. reflexivity.
      * exists x0. exact mflip.

    + destruct flopstarts. destruct H.
      ffseg H0. rename H0 into mflop.
      destruct H. injection H; clear H; intros; subst. rename a into m.
      destruct flipkeeps.
      clear H.
      rename H0 into mnotfloptrue.
      assert (mnotfloptrue' := mnotfloptrue (m::nil)). clear mnotfloptrue.
      apply mnotfloptrue'.
      * exists t. reflexivity.
      * exact mflop.

    + destruct flipkeeps as [ tflip _ ].
      destruct flopkeeps as [ tflop _ ].
      apply IHt.
      * exact tflip.
      * exact tflop.

  - (*bit equal to the case above*)
    induction t.
    {
      unfold prop_to_trace_prop, ltl.tillnow in H.
      destruct H. destruct H. destruct H. nilapp H.
      destruct H1. destruct H1. destruct H1. destruct H1. nilapp H1.
      compute in H3. contradiction H3.
    }

    destruct (luf.tillnow_unfold _ _ _ _ H) as [ flipstarts | flipkeeps ]; clear H;
      destruct (luf.tillnow_unfold _ _ _ _ H0) as [ flopstarts | flopkeeps ]; clear H0.

    + destruct flipstarts. destruct H. destruct flopstarts. destruct H1.
      ffseg H0.
      ffseg H2.
      compute in H0. compute in H2.
      magic_case (m.to_ff m); rewrite H3 in *; [ | contradiction H0 ].
      magic_case (m.to_ff m0); rewrite H4 in *; [ | contradiction H2 ].
      destruct s0, s; destruct x, x0; try contradiction.
      {
      destruct H; injection H; clear H; intros; subst.
      destruct H1; injection H; clear H; intros; subst.
      rewrite H3 in H4.
      injit H4. contradiction.
      }
      {
      destruct H; injection H; clear H; intros; subst.
      destruct H1; injection H; clear H; intros; subst.
      rewrite H3 in H4.
      injit H4. contradiction.
      }

    + destruct flipstarts. destruct H.
      ffseg H0. rename H0 into mflip.
      destruct H. injection H; clear H; intros; subst. rename a into m.
      destruct flopkeeps.
      clear H.
      rename H0 into mnotfliptrue.
      assert (mnotfliptrue' := mnotfliptrue (m::nil)). clear mnotfliptrue.
      apply mnotfliptrue'.
      * exists t. reflexivity.
      * exact mflip.

    + destruct flopstarts. destruct H.
      ffseg H0. rename H0 into mflop.
      destruct H. injection H; clear H; intros; subst. rename a into m.
      destruct flipkeeps.
      clear H.
      rename H0 into mnotfloptrue.
      assert (mnotfloptrue' := mnotfloptrue (m::nil)). clear mnotfloptrue.
      apply mnotfloptrue'.
      * exists t. reflexivity.
      * exists x0. exact mflop.

    + destruct flipkeeps as [ tflip _ ].
      destruct flopkeeps as [ tflop _ ].
      apply IHt.
      * exact tflip.
      * exact tflop.
Qed.

Lemma propagates_correct
           {p a res} (a_prop_p : propagates a res p)
           {s} (s_is_a : segment_of_action_with_res a res s)
           {t} (t_is_p : prop_to_trace_prop p t) :
  prop_to_trace_prop p (s ++ t).
Proof.
  unfold propagates in a_prop_p.
  destruct a.

  - destruct res; [ contradiction a_prop_p | ].
    destruct p; [ contradiction a_prop_p | ]. clear a_prop_p.
    ffseg s_is_a. unfold app. apply luf.tillnow_fold.
    + intros.
      intro. ffseg H1. destruct H0. injection H0; clear H0; intros; subst.
      compute in s_is_a. compute in H1.
      magic_case (m.to_ff m); rewrite H0 in *.
      * desig. destruct x0; [ | solve [ contradiction H1 ] ].
        destruct s_is_a, H1. subst f. discriminate H4.
      * solve [ contradiction H1 ].
    + right. split; [ exact t_is_p | ].
      intros. intro.
      ffseg H0. destruct H. injection H; clear H; intros; subst.
      compute in s_is_a. compute in H0.
      magic_case (m.to_ff m); rewrite H in *; [ | contradiction H0 ].
      desig. destruct x0; [ | solve [ contradiction H0 ] ].
      destruct s_is_a, H0. subst f. discriminate H3.

  - contradiction a_prop_p.
Qed.

Lemma back_propagates_correct
           {p a res} (a_prop_p : back_propagates a res p)
           {s} (s_is_a : segment_of_action_with_res a res s)
           {t} (t_is_p : prop_to_trace_prop p (s ++ t)) :
  prop_to_trace_prop p t.
Proof.
  unfold back_propagates in a_prop_p.
  unfold propagates in a_prop_p.
  destruct a.

  - destruct res; [ contradiction a_prop_p | ].
    destruct p; [ contradiction a_prop_p | ]. subst h0.
    ffseg s_is_a. unfold app in t_is_p.
    assert (Hx := luf.tillnow_unfold _ _ _ _ t_is_p); clear t_is_p; destruct Hx as [ t_is_p | t_is_p ].
    + destruct t_is_p. destruct H. ffseg H0.
      destruct H. injit H.
      compute in s_is_a. compute in H0.
      magic_case (m.to_ff m); rewrite H in *.
      * desig. destruct x; [ solve [ contradiction H0 ] | ].
        solve [ contradiction s_is_a ].
      * solve [ contradiction s_is_a ].
    + destruct t_is_p. exact H.

  - contradiction a_prop_p.
Qed.

Definition isflip h := prop_to_trace_prop (ff_prop_isflip h).
Definition isflop h := prop_to_trace_prop (ff_prop_isflop h).

Definition plain_action := ea.plain_action action_res segment_of_action_with_res.
Axiom ff_action_flip_ :
  forall h,
    plain_action
      (fun t => prop_to_trace_prop (ff_prop_isflop h) t)
      (ff_action_flip h).
Axiom ff_action_flop_ :
  forall h,
    plain_action
      (fun t => prop_to_trace_prop (ff_prop_isflip h) t)
      (ff_action_flop h).

Definition flip h :
  ior.IO
    (fun t : list w.msg => prop_to_trace_prop (ff_prop_isflop h) t)
    (action_res (ff_action_flip h))
    (fun (res : action_res (ff_action_flip h)) (s t : list w.msg) =>
       segment_of_action_with_res (ff_action_flip h) res s /\
       ((exists b, res = ok b)-> prop_to_trace_prop (ff_prop_isflip h) (s ++ t))).
Proof.
  refine (io.IO_weaken (ff_action_flip_ h) _ _).
  intros. destruct H.
  split; [ exact H0 | ].
  intro. destruct H1. subst a.
  unfold prop_to_trace_prop, ltl.tillnow.
  exists nil. exists (s ++ t). split; [ reflexivity | ].
  split.
  - exists s.
    split; [ exists t; reflexivity | ].
    exists x0. exact H0.
  - intros. intro.
    ffseg H0.
    ffseg H3.
    simpl in H2. destruct H2.
    destruct H1. nilapp H1.
    injit H2.
    simpl in H0. simpl in H3.
    magic_case (m.to_ff m); rewrite H2 in *.
    + destruct s. destruct x1; [ contradiction H3 | contradiction H0 ].
    + contradiction H0.
Defined.

Definition flop h :
  ior.IO
    (fun t : list w.msg => prop_to_trace_prop (ff_prop_isflip h) t)
    (action_res (ff_action_flop h))
    (fun (res : action_res (ff_action_flop h)) (s t : list w.msg) =>
       segment_of_action_with_res (ff_action_flop h) res s /\
       prop_to_trace_prop (ff_prop_isflop h) (s ++ t)).
Proof.
  refine (io.IO_weaken (ff_action_flop_ h) _ _).
  intros. destruct H.
  split; [ exact H0 | ].
  unfold prop_to_trace_prop, ltl.tillnow.
  exists nil. exists (s ++ t). split; [ reflexivity | ].
  split.
  - exists s.
    split; [ exists t; reflexivity | ].
    destruct a. exact H0.
  - intros. intro.
    ffseg H0.
    ffseg H3.
    simpl in H2. destruct H2.
    destruct H1. nilapp H1.
    injit H2.
    simpl in H0. simpl in H3.
    magic_case (m.to_ff m); rewrite H2 in *.
    + destruct s. destruct x1; [ contradiction H0 | contradiction H3 ].
    + contradiction H0.
Defined.

Definition action_to_IO :
  forall (a : action),
    { pre_post : _ & (ior.IO (fst pre_post) (action_res a) (snd pre_post) *
                      (forall res s t,
                         (snd pre_post) res s t ->
                         segment_of_action_with_res a res s))%type}.
Proof.
  intro. magic_case a.
  - refine (let Hx := io.IO_weaken
                        (flip h)
                        (fun res s t => segment_of_action_with_res (ff_action_flip h) res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); solve [ auto ].
    + intros. destruct H1 as [ H1 _ ]. exact H1.
  - refine (let Hx := io.IO_weaken
                        (flop h)
                        (fun res s t => segment_of_action_with_res (ff_action_flop h) res s)
                        _
            in _).
    Unshelve.
    + refine (existT _ (_, _) (Hx, _)); solve [ auto ].
    + intros. destruct H1 as [ H1 _ ]. exact H1.
Defined.

Corollary neg_propagation :
  forall {p a res},
    propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p (s ++ t) ->
        ~prop_to_trace_prop p t.
Proof.
  intros. intro. elim H1.
  exact (propagates_correct H H0 H2).
Qed.

Corollary neg_back_propagation :
  forall {p a res},
    back_propagates a res p ->
    forall {s},
      segment_of_action_with_res a res s ->
      forall {t},
        ~prop_to_trace_prop p t ->
        ~prop_to_trace_prop p (s ++ t).
Proof.
  intros. intro. elim H1. apply (back_propagates_correct H H0 H2).
Qed.

End ElemFF.
